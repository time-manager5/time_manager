from datetime import datetime, timedelta
from time import sleep as wait

from flask import Blueprint, render_template, request, flash, jsonify, make_response
from flask_login import login_required, current_user
from sqlalchemy.util.topological import sort

from .models import Note
from . import db
import json

views = Blueprint('views', __name__)


@views.route('/', methods=['GET', 'POST'])
@login_required
def home():
    if request.method == 'POST':
        try:
            first_date = datetime.strptime(request.form.get('first_date'), '%Y-%m-%d')
            second_date = datetime.strptime(request.form.get('second_date'), '%Y-%m-%d')
            name_work = str(request.form.get('name_work'))
        except:
            flash('Input data!', category='error')
        else:
            count_days = (second_date - first_date).days
            new_note = Note(first_date=first_date,
                            second_date=second_date,
                            user_id=current_user.id,
                            name_work=name_work,
                            count_days=count_days)
            db.session.add(new_note)
            db.session.commit()
            flash('Note added!', category='success')
    sorted_dates = db.session.query(Note).filter_by(user_id=current_user.id).all()
    for i, _ in enumerate(sorted_dates):
        for j in range(i+1, len(sorted_dates)):
            if sorted_dates[i].first_date.timetuple().tm_year == sorted_dates[j].first_date.timetuple().tm_year and\
            sorted_dates[i].first_date.timetuple().tm_mon == sorted_dates[j].first_date.timetuple().tm_mon or \
            abs(sorted_dates[i].first_date.timetuple().tm_mon - sorted_dates[j].first_date.timetuple().tm_mon) == 1 or\
            abs(sorted_dates[j].first_date.timetuple().tm_mon - sorted_dates[i].first_date.timetuple().tm_mon) == 1:
                if 0>sorted_dates[i].first_date.timetuple().tm_yday-sorted_dates[j].first_date.timetuple().tm_yday>-3:
                    sorted_dates[j].first_date = sorted_dates[i].first_date
                elif 3>sorted_dates[i].first_date.timetuple().tm_yday-sorted_dates[j].first_date.timetuple().tm_yday>0:
                    sorted_dates[i].first_date = sorted_dates[j].first_date

    for i, _ in enumerate(sorted_dates):
        for j in range(i, len(sorted_dates)):
            if sorted_dates[i].first_date.timetuple().tm_year == sorted_dates[j].second_date.timetuple().tm_year and \
            sorted_dates[i].first_date.timetuple().tm_mon == sorted_dates[j].second_date.timetuple().tm_mon or\
            abs(sorted_dates[i].first_date.timetuple().tm_mon - sorted_dates[j].second_date.timetuple().tm_mon) == 1 or\
            abs(sorted_dates[j].first_date.timetuple().tm_mon - sorted_dates[i].second_date.timetuple().tm_mon) == 1:
                wait(5)
                if -3 <sorted_dates[i].first_date.timetuple().tm_yday - sorted_dates[j].second_date.timetuple().tm_yday<0:
                    sorted_dates[j].second_date = sorted_dates[i].first_date
                elif -3<sorted_dates[j].first_date.timetuple().tm_yday - sorted_dates[i].second_date.timetuple().tm_yday<0:
                    sorted_dates[i].second_date = sorted_dates[j].first_date
                elif 0<sorted_dates[i].first_date.timetuple().tm_yday - sorted_dates[j].second_date.timetuple().tm_yday<3:
                    sorted_dates[i].first_date = sorted_dates[j].second_date
                elif 0<sorted_dates[j].first_date.timetuple().tm_yday - sorted_dates[i].second_date.timetuple().tm_yday<3:
                    sorted_dates[j].first_date = sorted_dates[i].second_date

    for i, _ in enumerate(sorted_dates):
        for j in range(i+1, len(sorted_dates)):
            if sorted_dates[i].second_date.timetuple().tm_year == sorted_dates[j].second_date.timetuple().tm_year and\
            sorted_dates[i].second_date.timetuple().tm_mon == sorted_dates[j].second_date.timetuple().tm_mon or \
            abs(sorted_dates[i].second_date.timetuple().tm_mon - sorted_dates[j].second_date.timetuple().tm_mon) == 1 or \
            abs(sorted_dates[j].second_date.timetuple().tm_mon - sorted_dates[i].second_date.timetuple().tm_mon) == 1:
                if 0>sorted_dates[i].second_date.timetuple().tm_yday-sorted_dates[j].second_date.timetuple().tm_yday>-3:
                    sorted_dates[j].second_date = sorted_dates[i].second_date
                elif 3>sorted_dates[i].second_date.timetuple().tm_yday-sorted_dates[j].second_date.timetuple().tm_yday>0:
                    sorted_dates[i].second_date=sorted_dates[j].second_date
    sorted_dates = db.session.query(Note).filter_by(user_id=current_user.id).all()
    sorted_first_dates = []
    sorted_second_dates = []
    dates_sorted = []
    for i, _ in enumerate(sorted_dates):
        sorted_first_dates.append(sorted_dates[i].first_date)
        sorted_second_dates.append(sorted_dates[i].second_date)
        dates_sorted.append(sorted_first_dates[i])
        dates_sorted.append(sorted_second_dates[i])
    dates_sorted = sorted(list(set(dates_sorted)))

    # stop_interval = []
    # for i, _ in enumerate(dates_sorted):
    #     if 0 < i < len(dates_sorted)-1:
    #         interval = dates_sorted[i] + timedelta(days=1)
    #         stop_interval.append(interval)

    interval_number = []
    for i in range(len(dates_sorted)-1):
        interval_number.append(i+1)

    return render_template("home.html",
                           user=current_user,
                           dates_sorted=dates_sorted, # {'dates_sorted': dates_sorted, 'stop_interval': stop_interval},
                           interval_number=interval_number)

@views.route('/print_intervals/<intervals>', methods=['GET', 'POST'])
@login_required
def installation(intervals):
    sorted_dates = db.session.query(Note).filter_by(user_id=current_user.id).all()
    for i, _ in enumerate(sorted_dates):
        for j in range(i + 1, len(sorted_dates)):
            if sorted_dates[i].first_date.timetuple().tm_year == sorted_dates[j].first_date.timetuple().tm_year and \
                    sorted_dates[i].first_date.timetuple().tm_mon == sorted_dates[j].first_date.timetuple().tm_mon or \
                    abs(sorted_dates[i].first_date.timetuple().tm_mon - sorted_dates[
                        j].first_date.timetuple().tm_mon) == 1 or \
                    abs(sorted_dates[j].first_date.timetuple().tm_mon - sorted_dates[
                        i].first_date.timetuple().tm_mon) == 1:
                if 0 > sorted_dates[i].first_date.timetuple().tm_yday - sorted_dates[
                    j].first_date.timetuple().tm_yday > -3:
                    sorted_dates[j].first_date = sorted_dates[i].first_date
                elif 3 > sorted_dates[i].first_date.timetuple().tm_yday - sorted_dates[
                    j].first_date.timetuple().tm_yday > 0:
                    sorted_dates[i].first_date = sorted_dates[j].first_date

    for i, _ in enumerate(sorted_dates):
        for j in range(i, len(sorted_dates)):
            if sorted_dates[i].first_date.timetuple().tm_year == sorted_dates[j].second_date.timetuple().tm_year and \
                    sorted_dates[i].first_date.timetuple().tm_mon == sorted_dates[j].second_date.timetuple().tm_mon or \
                    abs(sorted_dates[i].first_date.timetuple().tm_mon - sorted_dates[
                        j].second_date.timetuple().tm_mon) == 1 or \
                    abs(sorted_dates[j].first_date.timetuple().tm_mon - sorted_dates[
                        i].second_date.timetuple().tm_mon) == 1:
                wait(5)
                if -3 < sorted_dates[i].first_date.timetuple().tm_yday - sorted_dates[
                    j].second_date.timetuple().tm_yday < 0:
                    sorted_dates[j].second_date = sorted_dates[i].first_date
                elif -3 < sorted_dates[j].first_date.timetuple().tm_yday - sorted_dates[
                    i].second_date.timetuple().tm_yday < 0:
                    sorted_dates[i].second_date = sorted_dates[j].first_date
                elif 0 < sorted_dates[i].first_date.timetuple().tm_yday - sorted_dates[
                    j].second_date.timetuple().tm_yday < 3:
                    sorted_dates[i].first_date = sorted_dates[j].second_date
                elif 0 < sorted_dates[j].first_date.timetuple().tm_yday - sorted_dates[
                    i].second_date.timetuple().tm_yday < 3:
                    sorted_dates[j].first_date = sorted_dates[i].second_date

    for i, _ in enumerate(sorted_dates):
        for j in range(i + 1, len(sorted_dates)):
            if sorted_dates[i].second_date.timetuple().tm_year == sorted_dates[j].second_date.timetuple().tm_year and \
                    sorted_dates[i].second_date.timetuple().tm_mon == sorted_dates[j].second_date.timetuple().tm_mon or \
                    abs(sorted_dates[i].second_date.timetuple().tm_mon - sorted_dates[
                        j].second_date.timetuple().tm_mon) == 1 or \
                    abs(sorted_dates[j].second_date.timetuple().tm_mon - sorted_dates[
                        i].second_date.timetuple().tm_mon) == 1:
                if 0 > sorted_dates[i].second_date.timetuple().tm_yday - sorted_dates[
                    j].second_date.timetuple().tm_yday > -3:
                    sorted_dates[j].second_date = sorted_dates[i].second_date
                elif 3 > sorted_dates[i].second_date.timetuple().tm_yday - sorted_dates[
                    j].second_date.timetuple().tm_yday > 0:
                    sorted_dates[i].second_date = sorted_dates[j].second_date
    sorted_first_dates = []
    sorted_second_dates = []
    dates_sorted = []
    for i, _ in enumerate(sorted_dates):
        sorted_first_dates.append(sorted_dates[i].first_date)
        sorted_second_dates.append(sorted_dates[i].second_date)
        dates_sorted.append(sorted_first_dates[i])
        dates_sorted.append(sorted_second_dates[i])
    dates_sorted = sorted(list(set(dates_sorted)))

    stop_interval = []
    for i, _ in enumerate(dates_sorted):
        if 0 < i < len(dates_sorted):
            interval = dates_sorted[i] + timedelta(days=1)
            stop_interval.append(interval)
    intervals = json.loads(intervals)
    return render_template("print_intervals.html",
                           user={"current_user": current_user, "dates_sorted": dates_sorted, "intervals": intervals},
                           dates_sorted ={"dates_sorted": dates_sorted, "intervals": intervals} # {'dates_sorted': dates_sorted, 'stop_interval': stop_interval},
                           # intervals=intervals,
                           )

@views.route('/delete-note', methods=['POST'])
def delete_note():
    note = json.loads(request.data)
    noteId = note['noteId']
    note = Note.query.get(noteId)
    if note:
        if note.user_id == current_user.id:
            db.session.delete(note)
            db.session.commit()

    return jsonify({})